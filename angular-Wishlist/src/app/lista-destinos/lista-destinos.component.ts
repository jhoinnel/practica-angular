import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  listaDetinos: DestinoViaje[];
  
  constructor() {
    this.listaDetinos = [];
   }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string): boolean{
    this.listaDetinos.push(new DestinoViaje(nombre, url));
    console.log(this.listaDetinos);
    return false;
  }

}
